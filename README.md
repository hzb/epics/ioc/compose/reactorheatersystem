# EPICS PID based reactor control

This project aimed on creating a production environment for the reactor heater. 
The crosstalk between the GWINSTEK power supply and the PID controller (pid.db) was not implemented. 

Used devices:
* K type thermocouple (Wago)
* GWINSTEK power supply
